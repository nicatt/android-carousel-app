package com.example.carousel;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    ImageView image;
    Map<Integer, Integer> imageList;
    int iterator = 1;
    Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.image = (ImageView) findViewById(R.id.carousel);
        this.imageList = new HashMap<>();
        this.imageList.put(1, R.drawable.vistula_1);
        this.imageList.put(2, R.drawable.vistula_2);
        this.imageList.put(3, R.drawable.vistula_3);
        this.imageList.put(4, R.drawable.vistula_4);
        this.imageList.put(5, R.drawable.vistula_5);
        this.imageList.put(6, R.drawable.vistula_6);
        this.imageList.put(7, R.drawable.vistula_7);
        this.imageList.put(8, R.drawable.vistula_8);
        this.imageList.put(9, R.drawable.vistula_9);
        this.imageList.put(10, R.drawable.vistula_10);

        ImageView play_pause = (ImageView) findViewById(R.id.play_pause);
        play_pause.setTag(android.R.drawable.ic_media_play);
    }

    public void play_pause(View view) {
        ImageView play_pause = (ImageView) findViewById(R.id.play_pause);

        //if the carousel is not playing
        if((int)play_pause.getTag() == android.R.drawable.ic_media_play) {
            this.runCarousel(1, this.imageList.size());
            play_pause.setImageResource(android.R.drawable.ic_media_pause);
            play_pause.setTag(android.R.drawable.ic_media_pause);
        }
        else {
            this.handler.removeCallbacksAndMessages(null);
            play_pause.setImageResource(android.R.drawable.ic_media_play);
            play_pause.setTag(android.R.drawable.ic_media_play);
        }
    }

    private void runCarousel(int i, int n) {
        MainActivity context = this;

        this.handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (i <= n) {
                    context.image.setImageResource(context.imageList.get(context.iterator));
                    context.iterator += 1;
                    runCarousel(context.iterator, n);
                }
                else {
                    context.iterator = 1;
                    runCarousel(context.iterator, n);
                }
            }
        }, 1500);
    }

    public void next(View view) {
        this.iterator = this.iterator < this.imageList.size() ? this.iterator += 1 : 1;
        this.image.setImageResource(this.imageList.get(this.iterator));
    }

    public void previous(View view) {
        this.iterator = this.iterator > 1 ? this.iterator -= 1 : 10;
        this.image.setImageResource(this.imageList.get(this.iterator));
    }
}